// ROS headers
#include "ros/ros.h"

// Local headers
#include "alb_requester.h"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "alb_services");
	ros::NodeHandle node_handle;
	AlbRequester requester(node_handle);
	ros::Rate rate(20);

	if (!requester.init()) {
		ROS_ERROR("[alb_services] Invalid initialization");
		return -1;
	}

	while (ros::ok()) {
		ros::spinOnce();
		rate.sleep();
	}

	return 0;
}