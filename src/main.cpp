// ROS headers
#include "ros/ros.h"

// Local headers
#include "alb_reader.h"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "alb_data");
	ros::NodeHandle node_handle;

	ROS_INFO("[alb_data] - Init node");
	AlbReader reader(node_handle);

	if (!reader.init()) {
		ROS_ERROR("[alb_data] Invalid initialization");
		return -1;
	}

	while (ros::ok()) {
		reader.parse();
		ros::spinOnce();
	}

	return 0;
}