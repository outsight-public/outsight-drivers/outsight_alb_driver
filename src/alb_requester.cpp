// Local headers
#include "alb_requester.h"
#include "alb_common.h"
#include "alb_curl_helper.h"

AlbRequester::AlbRequester(ros::NodeHandle &node)
{
	defineServices(node);
}

bool AlbRequester::init(void)
{
	// Define the curl address.
	std::string ip_v4;
	ros::NodeHandle private_node("~");

	if (!private_node.hasParam(ALBCommon::k_alb_ip)) {
		ROS_ERROR("[AlbRequester] IP address not defined");
		return false;
	}

	private_node.getParam(ALBCommon::k_alb_ip, ip_v4);
	ip_address = ip_v4;

	return true;
}

void AlbRequester::defineServices(ros::NodeHandle &node)
{
	// Processing services.
	processing_service_restart = node.advertiseService(ALBCommon::k_alb_processing_restart,
							   &AlbRequester::restartProcessingCallback, this);
	processing_service_stop =
		node.advertiseService(ALBCommon::k_alb_processing_stop, &AlbRequester::stopProcessingCallback, this);
	processing_service_get_config =
		node.advertiseService(ALBCommon::k_alb_processing_get_config, &AlbRequester::getConfigCallback, this);
	processing_service_put_config =
		node.advertiseService(ALBCommon::k_alb_processing_put_config, &AlbRequester::putConfigCallback, this);

	// Storage services.
	storage_service_download =
		node.advertiseService(ALBCommon::k_alb_storage_download, &AlbRequester::downloadFileCallback, this);
	storage_service_upload =
		node.advertiseService(ALBCommon::k_alb_storage_upload, &AlbRequester::uploadFileCallback, this);
	storage_service_list =
		node.advertiseService(ALBCommon::k_alb_storage_list, &AlbRequester::listFilesCallback, this);
}

bool AlbRequester::restartProcessingCallback(std_srvs::Trigger::Request &request, std_srvs::Trigger::Response &response)
{
	AlbCurlHelper helper(ip_address);

	if (!helper.executeProcessing(AlbCurlHelper::processing_service_e::Restart)) {
		response.success = false;
		response.message = helper.getErrorMessage();
		return true;
	}

	AlbCurlHelper helperRun(ip_address);
	bool running = false;
	if (!helperRun.isProcessingRunning(running)) {
		response.success = false;
		response.message = helperRun.getErrorMessage();
		return true;
	}

	if (running) {
		response.success = true;
		response.message = std::string("Processing successfully restarted on ALB.");
	} else {
		response.success = false;
		response.message = std::string("Unable to restart processing on ALB.");
	}

	return true;
}

bool AlbRequester::stopProcessingCallback(std_srvs::Trigger::Request &request, std_srvs::Trigger::Response &response)
{
	AlbCurlHelper helper(ip_address);

	if (!helper.executeProcessing(AlbCurlHelper::processing_service_e::Stop)) {
		response.success = false;
		response.message = helper.getErrorMessage();
		return true;
	}

	AlbCurlHelper helperRun(ip_address);
	bool running = false;
	if (!helperRun.isProcessingRunning(running)) {
		response.success = false;
		response.message = helperRun.getErrorMessage();
		return true;
	}

	if (!running) {
		response.success = true;
		response.message = std::string("Processing successfully stopped on ALB.");
	} else {
		response.success = false;
		response.message = std::string("Unable to stop processing on ALB.");
	}

	return true;
}

bool AlbRequester::getConfigCallback(outsight_alb_driver::AlbConfig::Request &request,
				     outsight_alb_driver::AlbConfig::Response &response)
{
	AlbCurlHelper helper(ip_address);

	if (!helper.executeProcessingConfig(AlbCurlHelper::processing_config_e::Get, request)) {
		response.success = false;
		response.message = helper.getErrorMessage();
		return true;
	}

	response.success = true;
	response.message = std::string("Successfully get the config.");

	return true;
}

bool AlbRequester::putConfigCallback(outsight_alb_driver::AlbConfig::Request &request,
				     outsight_alb_driver::AlbConfig::Response &response)
{
	AlbCurlHelper helper(ip_address);

	if (!helper.executeProcessingConfig(AlbCurlHelper::processing_config_e::Put, request)) {
		response.success = false;
		response.message = helper.getErrorMessage();
		return true;
	}

	response.success = true;
	response.message = std::string("Successfully put the config.");

	return true;
}

bool AlbRequester::downloadFileCallback(outsight_alb_driver::AlbFile::Request &request,
					outsight_alb_driver::AlbFile::Response &response)
{
	AlbCurlHelper helper(ip_address);

	if (!helper.executeStorage(AlbCurlHelper::storage_service_e::Download, request)) {
		response.success = false;
		response.message = helper.getErrorMessage();
		return true;
	}

	response.success = true;
	response.message = std::string("File successfully downloaded.");

	return true;
}

bool AlbRequester::uploadFileCallback(outsight_alb_driver::AlbFile::Request &request,
				      outsight_alb_driver::AlbFile::Response &response)
{
	AlbCurlHelper helper(ip_address);

	if (!helper.executeStorage(AlbCurlHelper::storage_service_e::Upload, request)) {
		response.success = false;
		response.message = helper.getErrorMessage();
		return true;
	}

	response.success = true;
	response.message = std::string("File successfully uploaded.");

	return true;
}

bool AlbRequester::listFilesCallback(outsight_alb_driver::AlbFile::Request &request,
				     outsight_alb_driver::AlbFile::Response &response)
{
	AlbCurlHelper helper(ip_address);

	if (!helper.executeStorage(AlbCurlHelper::storage_service_e::List, request)) {
		response.success = false;
		response.message = helper.getErrorMessage();
		return true;
	}

	response.success = true;
	response.message = std::string("Files successfully displayed.");

	return true;
}