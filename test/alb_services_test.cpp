// Standard headers
#include <memory>
#include <thread>

// GTest headers
#include <gtest/gtest.h>

// ROS headers
#include "ros/ros.h"
#include "std_srvs/Trigger.h"

// Local headers
#include "alb_common.h"
#include "alb_requester.h"
#include "outsight_alb_driver/AlbConfig.h"
#include "outsight_alb_driver/AlbFile.h"

// Constant testing definition.
constexpr const char *k_invalid_service = "invalid_service";

/// Class to test the ALB services.
class AlbServicesTest : public ::testing::Test {
    protected:
	void SetUp() override
	{
		requester = std::make_unique<AlbRequester>(node);
	}

	void TearDown() override
	{
		// Sleep to correctly delete the ROS services.
		ros::Duration(0.5).sleep();
	}

	/// \brief Function to check if a service can be called.
	void testServiceCall(const std::string &service_name)
	{
		// Check that the callback is called.
		ASSERT_TRUE(ros::service::waitForService(service_name, ros::Duration(1)));
		ASSERT_TRUE(ros::service::call(service_name, request, response));
	}

    protected:
	ros::NodeHandle node;
	std::unique_ptr<AlbRequester> requester;
	std_srvs::Trigger::Request request;
	std_srvs::Trigger::Response response;
};

/// \brief Test to check that invalid service call should fail.
TEST_F(AlbServicesTest, invalidService)
{
	ASSERT_FALSE(ros::service::exists(k_invalid_service, true));
	ASSERT_FALSE(ros::service::waitForService(k_invalid_service, ros::Duration(1)));
	ASSERT_FALSE(ros::service::call(k_invalid_service, request, response));
}

/// \brief Test if the processing services are defined.
TEST_F(AlbServicesTest, processingServicesDefined)
{
	ASSERT_TRUE(ros::service::exists(ALBCommon::k_alb_processing_restart, true));
	ASSERT_TRUE(ros::service::exists(ALBCommon::k_alb_processing_stop, true));
	ASSERT_TRUE(ros::service::exists(ALBCommon::k_alb_processing_get_config, true));
	ASSERT_TRUE(ros::service::exists(ALBCommon::k_alb_processing_put_config, true));
}

/// \brief Test if the processing services are called.
TEST_F(AlbServicesTest, processingServicesCalled)
{
	testServiceCall(ALBCommon::k_alb_processing_restart);
	testServiceCall(ALBCommon::k_alb_processing_stop);
}

/// \brief Test if the processing configuration services are called.
TEST_F(AlbServicesTest, processingConfigServicesCalled)
{
	outsight_alb_driver::AlbConfig srv;
	ASSERT_TRUE(ros::service::waitForService(ALBCommon::k_alb_processing_get_config, ros::Duration(1)));
	ASSERT_TRUE(ros::service::call(ALBCommon::k_alb_processing_get_config, srv));

	ASSERT_TRUE(ros::service::waitForService(ALBCommon::k_alb_processing_put_config, ros::Duration(1)));
	ASSERT_TRUE(ros::service::call(ALBCommon::k_alb_processing_put_config, srv));
}

/// \brief Test if the storage services are defined.
TEST_F(AlbServicesTest, storageServicesDefined)
{
	ASSERT_TRUE(ros::service::exists(ALBCommon::k_alb_storage_download, true));
	ASSERT_TRUE(ros::service::exists(ALBCommon::k_alb_storage_upload, true));
	ASSERT_TRUE(ros::service::exists(ALBCommon::k_alb_storage_list, true));
}

/// \brief Test if the storage services are called.
TEST_F(AlbServicesTest, storageServicesCalled)
{
	outsight_alb_driver::AlbFile srv;
	ASSERT_TRUE(ros::service::waitForService(ALBCommon::k_alb_storage_download, ros::Duration(1)));
	ASSERT_TRUE(ros::service::call(ALBCommon::k_alb_storage_download, srv));

	ASSERT_TRUE(ros::service::waitForService(ALBCommon::k_alb_storage_upload, ros::Duration(1)));
	ASSERT_TRUE(ros::service::call(ALBCommon::k_alb_storage_upload, srv));

	ASSERT_TRUE(ros::service::waitForService(ALBCommon::k_alb_storage_list, ros::Duration(1)));
	ASSERT_TRUE(ros::service::call(ALBCommon::k_alb_storage_list, srv));
}

int main(int argc, char **argv)
{
	testing::InitGoogleTest(&argc, argv);
	ros::init(argc, argv, "alb_services_test");

	std::thread t([] {
		while (ros::ok()) {
			ros::spin();
		}
	});

	auto res = RUN_ALL_TESTS();
	ros::shutdown();

	return res;
}