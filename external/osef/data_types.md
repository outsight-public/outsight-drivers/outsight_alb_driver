---
description: Breakdown of each data type listed within the OSEF Data
---

# Augmented Data Types

This section describes in more details the Data Types that can be found in the Augmented Data output stream.

{% hint style="info" %}
In order to conveniently get access to the data from the OSEF stream, you can use a sample parser in Python available [here](https://gitlab.com/outsight-public/alb-software-package/-/blob/master/software-utilities/python/osefParser.py).
{% endhint %}

The type names listed below under the "Parser Name" field are those used in the above sample parser code, so you can directly refer to them if you're building Python Application Software.

The OSEF Stream can be seen as a data tree with each Node being either a Leaf or a Branch. A Leaf directly contains the value of the field with the specified format whereas a Branch other Sub-TLVs \(either Leaves or other Branches\).

{% hint style="warning" %}
All data elements use little-endian representation, unless specified otherwise.
{% endhint %}

The following fields are streamed for each LiDAR frame:

### Augmented Cloud

An augmented cloud represents a cloud of points. Each point can have attributes, all of which are optional, which
describe its position, physical property, movement, etc...


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 1 | OSEF\_TYPE\_AUGMENTED\_CLOUD | Branch | Augmented Cloud |

**This type contains the following Sub-TLVs:**

| Type ID | Name | Node | Parser Name | Instances |
| :--- | :--- | :--- | :--- | :--- |
| 2 | [OSEF\_TYPE\_NUMBER\_OF\_POINTS](data_types.md#number-of-points) | Branch | Number of Points | exactly one |
| 14 | [OSEF\_TYPE\_NUMBER\_OF\_LAYERS](data_types.md#number-of-layers) | Branch | Number of layers | exactly one |
| 3 | [OSEF\_TYPE\_SPHERICAL\_COORDINATES](data_types.md#spherical-coordinates) | Branch | Spherical Coordinates | none or one |
| 4 | [OSEF\_TYPE\_REFLECTIVITIES](data_types.md#reflectivities) | Branch | Reflectivities | none or one |
| 5 | [OSEF\_TYPE\_BACKGROUND\_FLAGS](data_types.md#background-flags) | Branch | Background Flags | none or one |
| 6 | [OSEF\_TYPE\_CARTESIAN\_COORDINATES](data_types.md#cartesian-coordinates) | Branch | Cartesian coordinates | none or one |
| 13 | [OSEF\_TYPE\_AZIMUTHS\_COLUMN](data_types.md#azimuths-column) | Branch | Azimuths Column | none or one |
| 15 | [OSEF\_TYPE\_CLOUD\_PROCESSING](data_types.md#cloud-processing) | Branch | Cloud Processing | none or one |
| 47 | [OSEF\_TYPE\_OBJECT\_ID\_32\_BITS](data_types.md#object-id-32-bits) | Branch | Object Id 32 bits | none or one |
| 31 | [OSEF\_TYPE\_CARTESIAN\_COORDINATES\_4F](data_types.md#cartesian-coordinates-4f) | Branch | Cartesian Coordinates 4F | none or one |
| 49 | [OSEF\_TYPE\_BACKGROUND\_BITS](data_types.md#background-bits) | Branch | Background bits | none or one |
| 50 | [OSEF\_TYPE\_GROUND\_PLANE\_BITS](data_types.md#ground-plane-bits) | Branch | Ground plane bits | none or one |
| 51 | [OSEF\_TYPE\_AZIMUTHS](data_types.md#azimuths) | Branch | Azimuths | none or one |
| 52 | [OSEF\_TYPE\_ELEVATIONS](data_types.md#elevations) | Branch | Elevations | none or one |
| 53 | [OSEF\_TYPE\_DISTANCES](data_types.md#distances) | Branch | Distances | none or one |

### Number of Points

Contains 32 bits unsigned int value representing the number of points in the point cloud.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 2 | OSEF\_TYPE\_NUMBER\_OF\_POINTS | Leaf | Number of Points |

### Spherical Coordinates [DEPRECATED USAGE]

Contains list of spherical single-precision float coordinates, three per point:

* azimuth : degrees, range \[ 0.0 .. 360.0 \]
* elevation : degrees, range \[ -90.0 .. +90.0 \]
* distance : meters, range \[ 0.0 ..  +inf \]


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 3 | OSEF\_TYPE\_SPHERICAL\_COORDINATES | Leaf | Spherical Coordinates |

### Reflectivities

Contains list of reflectivity values expressed as an unsigned 8-bits integer.

Diffuse reflectors report values from 0 to 100 for reflectivities from 0% to 100%.

Retroreflectors report values from 101 to 255, where 255 represents an ideal reflection.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 4 | OSEF\_TYPE\_REFLECTIVITIES | Leaf | Reflectivities |

### Background Flags [DEPRECATED USAGE]

Contains a list of boolean values encoded on an unsigned 8-bits integer:

* 0 means the point is not part of the background
* all other values mean the point is part of the background

The background is defined as the objects which were frequently detected in the past.
On the contrary the foreground is composed of objects that were not seen before (i.e. they appeared recently and/or
they are moving)

DEPRECATED: see \"Background bits\"


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 5 | OSEF\_TYPE\_BACKGROUND\_FLAGS | Leaf | Background Flags |

### Cartesian coordinates

Contains list of spherical single-precision float coordinates, three per point:

* x : meters, range \[ -inf .. +inf \]
* y : meters, range \[ -inf .. +inf \]
* z : meters, range \[ -inf .. +inf \]

The coordinate system is not specified.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 6 | OSEF\_TYPE\_CARTESIAN\_COORDINATES | Leaf | Cartesian coordinates |

### Cloud Frame

| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 11 | OSEF\_TYPE\_CLOUD\_FRAME | Branch | Cloud Frame |

**This type contains the following Sub-TLVs:**

| Type ID | Name | Node | Parser Name | Instances |
| :--- | :--- | :--- | :--- | :--- |
| 1 | [OSEF\_TYPE\_AUGMENTED\_CLOUD](data_types.md#augmented-cloud) | Branch | Augmented Cloud | exactly one |
| 16 | [OSEF\_TYPE\_RANGE\_AZIMUTH](data_types.md#range-azimuth) | Branch | Range Azimuth | exactly one |
| 41 | [OSEF\_TYPE\_TIMESTAMP\_LIDAR\_VELODYNE](data_types.md#timestamp-lidar-velodyne) | Branch | Timestamp Lidar Velodyne | none or one |

### Timestamp Microsecond

Describes a unix timestamp with microsecond precision, same as struct timeval of UNIX <sys/time.h>.
Contains concatenation of:
* UNIX time in seconds,   unsigned 32-bits integer, range \[ 0 .. 2^32 \[
* remaining microseconds, unsigned 32-bits integer, range \[ 0 .. 1000000 \[


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 12 | OSEF\_TYPE\_TIMESTAMP\_MICROSECOND | Leaf | Timestamp Microsecond |

### Azimuths Column

Contains list of azimuth values in degrees expressed as single-precision floats.

This azimuth value is computed before corrections due to:

  * The time between each laser firing
  * The correction when lasers of the same firing sequence are not aligned

The range is \[ 0.0 .. 360.0 \[


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 13 | OSEF\_TYPE\_AZIMUTHS\_COLUMN | Leaf | Azimuths Column |

### Number of layers

Contains 32 bits unsigned int value representing the number of layers of the point cloud.

0 indicates that this cloud does not have a 2D structure


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 14 | OSEF\_TYPE\_NUMBER\_OF\_LAYERS | Leaf | Number of layers |

### Cloud Processing

Contains a 64 bits bitfield, representing the processing that have been applied.

If no bit are set this means that no processing have been done on this cloud.

* Bit 0 : The background points have been removed


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 15 | OSEF\_TYPE\_CLOUD\_PROCESSING | Leaf | Cloud Processing |

### Range Azimuth

Range of azimuth values for the points contained in a given lidar packet.

Contains exactly two 32 bits floats. The first one marks the beginning of the range, the second the end.

  The range is \[ 0.0 .. 360.0 \[


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 16 | OSEF\_TYPE\_RANGE\_AZIMUTH | Leaf | Range Azimuth |

### Bounding Boxes Array

Contains list of bounding boxes dimensions, four per box:
* x_min : percentage of the image size, 32-bits floating-point, range \[ 0 .. 1 \]
* y_min : percentage of the image size, 32-bits floating-point, range \[ 0 .. 1 \]
* x_max : percentage of the image size, 32-bits floating-point, range \[ 0 .. 1 \]
* y_max : percentage of the image size, 32-bits floating-point, range \[ 0 .. 1 \]


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 17 | OSEF\_TYPE\_BOUNDING\_BOXES\_ARRAY | Leaf | Bounding Boxes Array |

### Class ID Array

Contains list of classes ID, each ID is encoded on a signed 32 bits integer.

Mapping between ID and class is given below:

* 0: UNKNOWN (class cannot yet be determined with confidence)
* 1: PERSON
* 2: LUGGAGE
* 3: TROLLEY
* 4: TRUCK
* 5: BUS
* 6: CAR
* 7: VAN
* 8: TWO_WHEELER
* 9: MASK
* 10: NO_MASK


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 18 | OSEF\_TYPE\_CLASS\_ID\_ARRAY | Leaf | Class ID Array |

### Timestamped data

| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 20 | OSEF\_TYPE\_TIMESTAMPED\_DATA | Branch | Timestamped data |

**This type contains the following Sub-TLVs:**

| Type ID | Name | Node | Parser Name | Instances |
| :--- | :--- | :--- | :--- | :--- |
| 12 | [OSEF\_TYPE\_TIMESTAMP\_MICROSECOND](data_types.md#timestamp-microsecond) | Branch | Timestamp Microsecond | exactly one |
| 11 | [OSEF\_TYPE\_CLOUD\_FRAME](data_types.md#cloud-frame) | Branch | Cloud Frame | none or one |
| 25 | [OSEF\_TYPE\_SCAN\_FRAME](data_types.md#scan-frame) | Branch | Scan Frame | none or one |

### Pose

Concatenation of 12 floats of 32-bits each, in this order:

- Tx, Ty and Tz, representing the translation vector T to get the position coordinates in meters
- Rxx, Ryx, Rzx, Rxy, Ryy, Rzy, Rxz, Ryz and Rzz represent the rotation matrix R (given column-wise) defining the orientation

As a consequence, considering a device in a pose defined by T and R, you can compute the coordinates Xabs in
the absolute referential from the coordinates Xrel in device referential using the vectorial formula:

```
Xabs = R * Xrel + T
```

with:

```
    [Rxx Rxy Rxz]
R = [Ryx Ryy Ryz]
    [Rzx Rzy Rzz]

    [Tx]
T = [Ty]
    [Tz]
```


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 24 | OSEF\_TYPE\_POSE | Leaf | Pose |

### Scan Frame

| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 25 | OSEF\_TYPE\_SCAN\_FRAME | Branch | Scan Frame |

**This type contains the following Sub-TLVs:**

| Type ID | Name | Node | Parser Name | Instances |
| :--- | :--- | :--- | :--- | :--- |
| 1 | [OSEF\_TYPE\_AUGMENTED\_CLOUD](data_types.md#augmented-cloud) | Branch | Augmented Cloud | exactly one |
| 24 | [OSEF\_TYPE\_POSE](data_types.md#pose) | Branch | Pose | none or one |
| 46 | [OSEF\_TYPE\_GEOGRAPHIC\_POSE](data_types.md#geographic-pose) | Branch | Geographic pose | none or one |
| 44 | [OSEF\_TYPE\_EGO\_MOTION](data_types.md#ego-motion) | Branch | Ego Motion | none or one |
| 26 | [OSEF\_TYPE\_TRACKED\_OBJECTS](data_types.md#tracked-objects) | Branch | Tracked Objects | none or one |
| 33 | [OSEF\_TYPE\_ZONES\_DEF](data_types.md#zones-def) | Branch | Zones Def | none or one |
| 38 | [OSEF\_TYPE\_ZONES\_OBJECTS\_BINDING](data_types.md#zones-objects-binding) | Branch | Zones Objects Binding | none or one |
| 48 | [OSEF\_TYPE\_ZONES\_OBJECTS\_BINDING\_32\_BITS](data_types.md#zones-objects-binding-32-bits) | Branch | Zones Objects Binding 32 bits | none or one |

### Tracked Objects

Properties of tracked objects, which includes centroid, bbox, speed...


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 26 | OSEF\_TYPE\_TRACKED\_OBJECTS | Branch | Tracked Objects |

**This type contains the following Sub-TLVs:**

| Type ID | Name | Node | Parser Name | Instances |
| :--- | :--- | :--- | :--- | :--- |
| 47 | [OSEF\_TYPE\_OBJECT\_ID\_32\_BITS](data_types.md#object-id-32-bits) | Branch | Object Id 32 bits | exactly one |
| 18 | [OSEF\_TYPE\_CLASS\_ID\_ARRAY](data_types.md#class-id-array) | Branch | Class ID Array | none or one |
| 28 | [OSEF\_TYPE\_SPEED\_VECTORS](data_types.md#speed-vectors) | Branch | Speed Vectors | none or one |
| 29 | [OSEF\_TYPE\_POSE\_ARRAY](data_types.md#pose-array) | Branch | Pose Array | none or one |
| 39 | [OSEF\_TYPE\_OBJECT\_PROPERTIES](data_types.md#object-properties) | Branch | Object Properties | none or one |

### Speed Vectors

Contains list of speed vectors for tracked objects, three per object:

* x : meters, range \[ -inf .. +inf \]
* y : meters, range \[ -inf .. +inf \]
* z : meters, range \[ -inf .. +inf \]

The coordinate system is not specified.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 28 | OSEF\_TYPE\_SPEED\_VECTORS | Leaf | Speed Vectors |

### Pose Array

Contains the poses of the tracked objects, 2 floats of 32-bits per object:

* Tx, Ty and Tz represent the translation vector T to get the position coordinates in meters.
* Rxx, Rxy, Rxz, Ryx, Ryy, Ryz, Rzx, Rzy and Rzz represent the rotation matrix R defining the orientation.

As a consequence, considering a device in a pose defined by T and R, you can compute the coordinates Xabs in
the absolute referential from the coordinates Xrel in device referential using the vectorial formula:

Xabs = R * Xrel + T


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 29 | OSEF\_TYPE\_POSE\_ARRAY | Leaf | Pose Array |

### Object Id

This type is deprecated, replaced by the 32 bits default Object Id (Type 47).


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 30 | OSEF\_TYPE\_OBJECT\_ID | Leaf | Object Id |

### Cartesian Coordinates 4F [DEPRECATED USAGE]

Alternative way to represent coordinates, where a fourth float is added.

It can be more efficient to construct if an application aligns the points to use SIMD on 128 bits words.

Contains list of cartesian single-precision float coordinates, four per point:

* x : meters, range \[ -inf .. +inf \]
* y : meters, range \[ -inf .. +inf \]
* z : meters, range \[ -inf .. +inf \]
* w : unused, for 128 bits alignment only


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 31 | OSEF\_TYPE\_CARTESIAN\_COORDINATES\_4F | Leaf | Cartesian Coordinates 4F |

### Spherical Coordinates 4F [DEPRECATED USAGE]

Alternative way to represent coordinates, where a fourth float is added.

It can be more efficient to construct if an application aligns the points to use SIMD on 128 bits words.

Contains list of spherical single-precision float coordinates, four per point:

* azimuth   : degrees, range \[ 0.0 .. 360.0 \]
* elevation : degrees, range \[ -90.0 .. +90.0 \]
* distance  : meters,  range \[ 0.0 ..  +inf \]
* w         : unused, for 128 bits alignment only


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 32 | OSEF\_TYPE\_SPHERICAL\_COORDINATES\_4F | Leaf | Spherical Coordinates 4F |

### Zones Def

Definition of the zones. They represent spatial areas of interest.

Their order is important, since the index is used to identify a zone in type 48.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 33 | OSEF\_TYPE\_ZONES\_DEF | Branch | Zones Def |

**This type contains the following Sub-TLVs:**

| Type ID | Name | Node | Parser Name | Instances |
| :--- | :--- | :--- | :--- | :--- |
| 34 | [OSEF\_TYPE\_ZONE](data_types.md#zone) | Branch | Zone | zero to many |

### Zone

Defines one zone.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 34 | OSEF\_TYPE\_ZONE | Branch | Zone |

**This type contains the following Sub-TLVs:**

| Type ID | Name | Node | Parser Name | Instances |
| :--- | :--- | :--- | :--- | :--- |
| 35 | [OSEF\_TYPE\_ZONE\_VERTICES](data_types.md#zone-vertices) | Branch | Zone Vertices | exactly one |
| 36 | [OSEF\_TYPE\_ZONE\_NAME](data_types.md#zone-name) | Branch | Zone Name | exactly one |
| 37 | [OSEF\_TYPE\_ZONE\_UUID](data_types.md#zone-uuid) | Branch | Zone UUID | none or one |

### Zone Vertices

Vertices of the polygon defining the zone.

They are defined on the ground, so the z coordinate is absent.

Contains list of cartesian single-precision float coordinates, two per point.

* x : meters, range \[ -inf .. +inf \]
* y : meters, range \[ -inf .. +inf \]

There must be at least 3 vertices, so at least 6 floats


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 35 | OSEF\_TYPE\_ZONE\_VERTICES | Leaf | Zone Vertices |

### Zone Name

User-defined name to the zone, do not use as unique identifier.

Encoded in ASCII, null terminated.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 36 | OSEF\_TYPE\_ZONE\_NAME | Leaf | Zone Name |

### Zone UUID

128 bits UUID of the zone in BIG-endian representation.

UUID 00112233-4455-6677-8899-aabbccddeeff is encoded as the bytes 00 11 22 33 44 55 66 77 88 99 aa bb cc dd ee ff.

It enables to keep the identity of a zone across renaming or resizing.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 37 | OSEF\_TYPE\_ZONE\_UUID | Leaf | Zone UUID |

### Zones Objects Binding [DEPRECATED USAGE]


Replaced by type 48.

Concatenation of 0 to N couples of:

* an unsigned 64-bits integer, representing the ID of an object \(see type 30\)
* an unsigned 32-bits integer, representing the index of the zone \(the n-th type 34 of type 33\)

Each object-zone couple means that the object is considered by the algorithm to be in the zone.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 38 | OSEF\_TYPE\_ZONES\_OBJECTS\_BINDING | Leaf | Zones Objects Binding |

### Object Properties

Properties of the object.

One byte per object, each bit of this can represent different properties.

* 1st bit: 1 if the object has a proper orientation \(like a cuboid\), 0 otherwise \(like a cylinder\)
* 2nd bit: 1 if the object has been seen in the last scan, 0 if it was not seen.
* 3rd bit: reserved for later use
* 4th bit: reserved for later use
* 5th bit: reserved for later use
* 6th bit: reserved for later use
* 7th bit: reserved for later use
* 8th bit: reserved for later use


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 39 | OSEF\_TYPE\_OBJECT\_PROPERTIES | Leaf | Object Properties |

### IMU Packet

Contains the following data:

* Time when the IMU packet was received
* Byte \[0,3\]: UNIX time in seconds,   unsigned 32-bits integer, range \[ 0 .. 2^32 \[
* Byte \[4,7\]: remaining microseconds, unsigned 32-bits integer, range \[ 0 .. 1000000 \[
* Acceleration vector
* Byte \[8,11\] x : meters / second^2, 32 bits float, range \[ -inf .. +inf \]
* Byte \[12,15\] y : meters / second^2, 32 bits float, range \[ -inf .. +inf \]
* Byte \[16,19\] z : meters / second^2, 32 bits float, range \[ -inf .. +inf \]
* Angular velocity vector
* Byte \[20,23\] x : degrees / second, 32 bits float, range \[ -inf .. +inf \]
* Byte \[24,27\] x : degrees / second, 32 bits float, range \[ -inf .. +inf \]
* Byte \[28,31\] x : degrees / second, 32 bits float, range \[ -inf .. +inf \]


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 40 | OSEF\_TYPE\_IMU\_PACKET | Leaf | IMU Packet |

### Timestamp Lidar Velodyne

Describes a timestamp with microsecond precision. This timestamp is in the lidar reference system, and uses the
Velodyne format. This means that this timestamp is the time spent since the beginning of the hour. Contains
concatenation of:

* UNIX time in seconds,   unsigned 32-bits integer, range \[ 0 .. 2^32 \[
* remaining microseconds, unsigned 32-bits integer, range \[ 0 .. 1000000 \[


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 41 | OSEF\_TYPE\_TIMESTAMP\_LIDAR\_VELODYNE | Leaf | Timestamp Lidar Velodyne |

### Pose Relative

Concatenation of 12 floats of 32-bits each, in this order:

- Tx, Ty and Tz, representing the translation vector T to get the position coordinates in meters
- Rxx, Ryx, Rzx, Rxy, Ryy, Rzy, Rxz, Ryz and Rzz, representing the rotation matrix R (given column-wise) defining the orientation

As a consequence, considering a device in a pose defined by T and R, you can compute the coordinates Xabs in
the absolute referential from the coordinates Xrel in device referential using the vectorial formula below.

This pose represents the movement of the device between two scans.
So the position of the current scan can be computed from the one of the previous scan using the following vectorial formula:

```
Xcurrent = R * Xprevious + T
```

with:

```
    [Rxx Rxy Rxz]
R = [Ryx Ryy Ryz]
    [Rzx Rzy Rzz]

    [Tx]
T = [Ty]
    [Tz]
```


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 42 | OSEF\_TYPE\_POSE\_RELATIVE | Leaf | Pose Relative |

### Gravity

Concatenation of 3 floats of 32-bits each \[x, y, z\], this is the direction of the gravity
in the acquisition sensor reference frame.

The vector is either normalised if valid, or \[0, 0, 0\] if invalid.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 43 | OSEF\_TYPE\_GRAVITY | Leaf | Gravity |

### Ego Motion

| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 44 | OSEF\_TYPE\_EGO\_MOTION | Branch | Ego Motion |

**This type contains the following Sub-TLVs:**

| Type ID | Name | Node | Parser Name | Instances |
| :--- | :--- | :--- | :--- | :--- |
| 42 | [OSEF\_TYPE\_POSE\_RELATIVE](data_types.md#pose-relative) | Branch | Pose Relative | exactly one |
| 45 | [OSEF\_TYPE\_PREDICTED\_POSITION](data_types.md#predicted-position) | Branch | Predicted Position | none or one |

### Predicted Position

Contains 3 floats, which are the cartesian coordinates of the predicted position in slam.

They are given in the LiDAR reference frame. The time of prediction is set at startup,
and defaults to 1 second.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 45 | OSEF\_TYPE\_PREDICTED\_POSITION | Leaf | Predicted Position |

### Geographic pose

Concatenation of 3 floats of 32-bits each \[lat, long, heading\] which represents the output geographic pose from
the relocation processing expressed in decimal degrees notation (latitude, longitude & heading).
* Single-precision floating-point (float32) : latitude in decimal degrees, range \[ -90.0 .. 90.0 \]
* Single-precision floating-point (float32) : longitude in decimal degrees, range \[ -180.0 .. 180.0 \]
* Single-precision floating-point (float32) : heading in decimal degrees, range \[ 0.0 .. 360.0 \[


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 46 | OSEF\_TYPE\_GEOGRAPHIC\_POSE | Leaf | Geographic pose |

### Object Id 32 bits

Contains list of object IDs found by tracking, as unsigned 32-bits integer.

Each ID represents a distinct tracked object.

The affectation of an ID to an object is arbitrary.

The special value 0 is used to mean 'no object'.
For instance, if the object ID of a point is 0, it means that this point does not belong to any object.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 47 | OSEF\_TYPE\_OBJECT\_ID\_32\_BITS | Leaf | Object Id 32 bits |

### Zones Objects Binding 32 bits

Concatenation of 0 to N couples of:

* an unsigned 32-bits integer, representing the ID of an object \(see 47\)
* an unsigned 32-bits integer, representing the index of the zone \(the n-th type 34 of type 33\)

Each object-zone couple means that the object is considered by the algorithm to be in the zone.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 48 | OSEF\_TYPE\_ZONES\_OBJECTS\_BINDING\_32\_BITS | Leaf | Zones Objects Binding 32 bits |

### Background bits

Contains a padded list of bits, 1 bit per point of the cloud. If the bit is set, the point is a background point.
The background is defined as the objects which were frequently detected in the past.
On the contrary the foreground is composed of objects that were not seen before
(i.e. they appeared recently and/or they are moving)


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 49 | OSEF\_TYPE\_BACKGROUND\_BITS | Leaf | Background bits |

### Ground plane bits

Contains a padded list of bits, 1 bit per point of the cloud. If the bit is set, the point belongs to the ground.


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 50 | OSEF\_TYPE\_GROUND\_PLANE\_BITS | Leaf | Ground plane bits |

### Azimuths

Contains list of azimuth coordinates.
The definition of this angle can be found [here](https://docs.outsight.ai/software/coordinate-conventions#lidar-reference-frame).

The azimuths are in degrees and expressed as single-precision floats. Several values are concatenated so the information of n points are given at once.

The range of each value is \[ 0.0 .. 360.0 \[. 360.0 \[


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 51 | OSEF\_TYPE\_AZIMUTHS | Leaf | Azimuths |

### Elevations

Contains list of elevation values in degrees expressed as single-precision floats.
The definition of the elevation can be found [here](https://docs.outsight.ai/software/coordinate-conventions#lidar-reference-frame)

The range is \[ -90.0 .. +90.0 \[


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 52 | OSEF\_TYPE\_ELEVATIONS | Leaf | Elevations |

### Distances

Contains list of distance values in meters expressed as single-precision floats.
The definition of the distance can be found [here](https://docs.outsight.ai/software/coordinate-conventions#lidar-reference-frame)

The range is \[ 0.0 .. +inf \[


| Type ID | Name | Node | Parser Name |
| :--- | :--- | :--- | :--- |
| 53 | OSEF\_TYPE\_DISTANCES | Leaf | Distances |

version: 1.0.1
version_date: 2021-08-10

