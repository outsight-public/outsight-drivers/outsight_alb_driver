#pragma once

class TcpStreamReader {
    public:
	// Define default IP and port for an ALB.
	static const char *default_ip;
	static const uint16_t default_port;

    public:
	// Connect to the stream on the specified address and port
	// returns:
	//  - 1 on sucess
	//  - 0 on non-fatal failure (ALB unavailable, processing is not started yet
	//  ?)
	//  - -1 on fatal failure (invalid IP)
	int connectToALB(const char *ipv4 = default_ip, const uint16_t port = default_port);

	// Poll the socket until a new frame is read
	// returns:
	//  - 1 on sucess (buffer now contains a valid frame)
	//  - 0 on end of file (stream ended)
	//  - -1 on fatal failure (corrupted stream)
	int getNextFrame(uint8_t *buffer, const size_t buffer_size);

	// Disconnect from ALB
	// returns:
	// - 0 on success
	// - <0 on failure (not connected or cannot close socket)
	int disconnectfromALB();

    private:
	int socket_fd = -1;
};
