#pragma once

#include <array>
#include <assert.h>
#include <cstdint>
#include <string.h>
#include <string>
#include <vector>

#include "tlvParser.h"

namespace TimestampMicroseconds
{
struct timestamp_s {
	uint32_t seconds = 0;
	uint32_t micro_seconds = 0;
};
static_assert(sizeof(timestamp_s) == 4 * 2);

timestamp_s castValue(const Tlv::tlv_s *leafTlv);
} // namespace TimestampMicroseconds

namespace ZonesObjectsBinding
{
#pragma pack(push, 1)
struct binding_s {
	uint64_t obj_pid = 0;
	uint32_t zone_index = 0;
};
#pragma pack(pop)

std::vector<binding_s> castValue(const Tlv::tlv_s *leafTlv);
} // namespace ZonesObjectsBinding

namespace ZonesObjectsBinding32Bits
{
#pragma pack(push, 1)
struct binding_s {
	uint32_t obj_pid = 0;
	uint32_t zone_index = 0;
};
#pragma pack(pop)

std::vector<binding_s> castValue(const Tlv::tlv_s *leafTlv);
std::vector<binding_s> castValue(const std::vector<ZonesObjectsBinding::binding_s> *binding64b);
} // namespace ZonesObjectsBinding32Bits

namespace ZoneName
{
std::string castValue(const Tlv::tlv_s *leafTlv);
}

namespace NumberOfObjects
{
uint32_t castValue(const Tlv::tlv_s *leafTlv);
}

namespace ObjectIds
{
std::vector<uint64_t> castValue(const Tlv::tlv_s *leafTlv);
}

namespace ObjectIds32Bits
{
std::vector<uint32_t> castValue(const Tlv::tlv_s *leafTlv);
std::vector<uint32_t> castValue(const std::vector<uint64_t> *object_ids_64b);
} // namespace ObjectIds32Bits

namespace Pose
{
const std::array<float, 3> castTranslationValue(const Tlv::tlv_s *leafTlv, size_t index = 0);
const std::array<float, 9> castRotationValue(const Tlv::tlv_s *leafTlv, size_t index = 0);
} // namespace Pose

namespace Cartesian
{
const std::array<float, 3> castValue(const Tlv::tlv_s *leafTlv, size_t pointIndex);
} // namespace Cartesian

// Template class to cast a TLV to a vector.
template <typename T>
std::vector<T> castValue(const Tlv::tlv_s *leafTlv)
{
	T *parsed = (T *)(leafTlv->getValue());
	size_t nb_of_elements(leafTlv->getLength() / sizeof(T));

	std::vector<T> data(nb_of_elements);
	for (size_t i = 0; i < nb_of_elements; ++i)
		data[i] = parsed[i];
	return data;
}