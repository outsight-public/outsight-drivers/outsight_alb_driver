^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package outsight_alb_driver
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.6.0 (2022-10-11)
------------------
Added
=======
* Configuration file parameter to roslaunch command

Changed
=======
* Update OSEF types to 1.6.1

Fixed
=======
* Fix TrackedObjects log (`MR #1 <https://gitlab.com/outsight-public/outsight-drivers/outsight_alb_driver/-/merge_requests/1>`__)


1.5.1 (2022-06-28)
------------------
Fixed
=======
* Fix odometry frame ID
* Always publish transform between map and fixed frame, and fix Pose frame

1.5.0 (2022-06-23)
------------------
Added
=======
* Publish zone definitions

Changed
=======
* Update OSEF types to 1.3.0

Fixed
=======
* Fix Odometry computation
* Use egomotion directly from ALB output

1.4.0 (2022-05-04)
------------------
Added
=======
* Add configuration parameter to reorder PointCloud2 to row-major order

Fixed
=======
* Fix dependency on navigation messages
* Fix computation of `row_step` for PointCloud2 message

1.3.0 (2022-04-29)
------------------
Added
=======
* Odometry publisher

Changed
=======
* AugmentedCloud message update
* Update OSEF library v1.2.1
* Frame system improved for robotic integration

Fixed
=======
* Use private node for parameters (`Issue #4 <https://gitlab.com/outsight-public/outsight-drivers/outsight_alb_driver/-/issues/4>`__)
* Fix file data reading in AlbCurlHelper
* Fix file uploading detected on ROS Melodic (`Issue #2 <https://gitlab.com/outsight-public/outsight-drivers/outsight_alb_driver/-/issues/2>`__)
* Fix checking defined storage command

1.2.2 (2022-03-14)
------------------
Fixed
=====
* Timereference microseconds conversion

1.2.1 (2022-02-10)
------------------
Fixed
=====
* Define fix loop rate at 20hz for services (`Issue #3 <https://gitlab.com/outsight-public/outsight-drivers/outsight_alb_driver/-/issues/3>`__)

1.2.0 (2021-12-9)
------------------
Added
=====
* Publish Outsight AugmentedCloud
* Add ROS documentation links

Changed
=======
* Update OSEF library

1.1.0 (2021-11-18)
------------------
Added
=====
* Add service to list available files on the ALB
* Add LaserScan conversion

Fixed
=====
* Fix normalized quaternion

1.0.0 (2021-11-2)
------------------
Added
=====
* First public release
* Node to parse messages from the ALB

  * PointCloud
  * Pose
  * TrackedObjects

* Services to interact with the ALB

  * Start/Stop processing
  * Get/Put configuration of the ALB
  * Upload/Download files on the ALB