#ifndef _ALB_PUBLISHER_H
#define _ALB_PUBLISHER_H

// ROS headers
#include "geometry_msgs/PoseStamped.h"
#include "nav_msgs/Odometry.h"
#include "ros/ros.h"

#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/transform_listener.h"

// Osef headers
#include "tlvParser.h"

// Local header
#include "outsight_alb_driver/Zones.h"

/// Class to publish ROS messages from the ALB.
class AlbPublisher {
    public:
	AlbPublisher(ros::NodeHandle &nodeHandle);

	/// \brief Publish messages from the ALB frame.
	void publish(const Tlv::tlv_s *frame);

    private:
	/// \brief Initialize the ROS publishers.
	void initPublishers(ros::NodeHandle &nodeHandle);

	/// \brief Initialize the transform frames.
	void initTransforms();

	/// \brief Build ALB zones when the processing starts.
	void buildZones();

	/// \brief Publish TimeReference message.
	void publishTimeReference(const Tlv::tlv_s *frame);

	/// \brief Publish pointCloud message.
	void publishPointCloud(const Tlv::tlv_s *frame);

	/// \brief Publish pose message.
	void publishPose(const Tlv::tlv_s *frame);

	/// \brief Publish tracked objects message.
	void publishTrackedObjects(const Tlv::tlv_s *frame);

	/// \brief Publish egomotion message.
	void publishEgomotion(const Tlv::tlv_s *frame);

	/// \brief Publish odometry message.
	void publishOdometry(const Tlv::tlv_s *frame);

	/// \brief Publish the augmented cloud message.
	void publishAugmentedCloud(const Tlv::tlv_s *frame);

	/// \brief Publish the zones message.
	void publishZones();

	/// \brief Update current position as the last position.
	void updatePose(const Tlv::tlv_s *frame);

	/// \brief Update used time stamp with the ALB time stamp.
	void updateTimeStamp(const Tlv::tlv_s *frame);

    private:
	/// \brief Broadcast the ALB fixed frame to sensor frame transform.
	void broadcastAlbTransform(const geometry_msgs::Pose &pose);

	/// \brief Broadcast the ALB map to fixed frame transform.
	void broadcastMapTransform(const geometry_msgs::Pose &pose);

    private:
	std::string fixed_frame_id;
	std::string base_frame_id;
	std::string sensor_frame_id;
	std::string map_frame_id;

	geometry_msgs::Pose last_pose;
	geometry_msgs::Pose current_pose;
	geometry_msgs::Pose first_pose;
	nav_msgs::Odometry odom_msg;
	bool pose_found;
	bool get_first_pose = false;
	outsight_alb_driver::Zones zones_msg;

	ros::Time ros_time;
	ros::Time alb_time;
	ros::Time last_timestamp;
	ros::Time current_timestamp;
	bool use_alb_time;

	bool use_colwise_order;

	ros::Publisher time_reference_publisher;
	ros::Publisher point_cloud_publisher;
	ros::Publisher pose_publisher;
	ros::Publisher tracked_objects_publisher;
	ros::Publisher egomotion_publisher;
	ros::Publisher odometry_publisher;
	ros::Publisher augmented_cloud_publisher;
	ros::Publisher zones_publisher;

	tf2::Transform base_to_lidar_tf;
	bool transform_broadcaster;
	bool use_base_frame;
};

#endif // _ALB_PUBLISHER_H