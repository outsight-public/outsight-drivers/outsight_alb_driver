#ifndef _ALB_COMMON_H
#define _ALB_COMMON_H

/// Namespace to define common ALB values.
namespace ALBCommon
{
// Constant definition of topics.
constexpr const char *k_alb_point_cloud = "alb/point_cloud";
constexpr const char *k_alb_pose = "alb/pose";
constexpr const char *k_alb_time_ref = "alb/time_reference";
constexpr const char *k_alb_objects = "alb/tracked_objects";
constexpr const char *k_alb_egomotion = "alb/egomotion";
constexpr const char *k_alb_odometry = "alb/odometry";
constexpr const char *k_alb_augmented_cloud = "alb/augmented_cloud";
constexpr const char *k_alb_zones = "alb/zones";

// Constant definition of IP configuration of the ALB.
constexpr const char *k_alb_ip = "/ip_address";
constexpr const char *k_alb_port = "/ip_port";

// Constant definition of frames.
constexpr const char *k_alb_fixed_frame_id = "frames/fixed_frame_id";
constexpr const char *k_alb_sensor_frame_id = "frames/sensor_frame_id";
constexpr const char *k_alb_base_frame_id = "frames/base_frame_id";
constexpr const char *k_alb_map_frame_id = "frames/map_frame_id";

// Constant definition of output parameters.
constexpr const char *k_alb_point_cloud_config = "output/point_cloud";
constexpr const char *k_alb_pose_config = "output/pose";
constexpr const char *k_alb_time_ref_config = "output/time_reference";
constexpr const char *k_alb_use_alb_time = "output/use_alb_time";
constexpr const char *k_alb_objects_config = "output/tracked_objects";
constexpr const char *k_alb_egomotion_config = "output/egomotion";
constexpr const char *k_alb_odometry_config = "output/odometry";
constexpr const char *k_alb_augmented_cloud_config = "output/augmented_cloud";
constexpr const char *k_alb_zones_config = "output/zones";
constexpr const char *k_alb_broadcast_tf_config = "output/broadcast_tf";
constexpr const char *k_alb_use_colwise_order = "output/use_colwise_order";

// Constant definition of services.
constexpr const char *k_alb_processing_restart = "alb/processing/restart";
constexpr const char *k_alb_processing_stop = "alb/processing/stop";
constexpr const char *k_alb_processing_get_config = "alb/processing/get_config";
constexpr const char *k_alb_processing_put_config = "alb/processing/put_config";

constexpr const char *k_alb_storage_download = "alb/storage/download";
constexpr const char *k_alb_storage_upload = "alb/storage/upload";
constexpr const char *k_alb_storage_list = "alb/storage/list";

// Constant definition for zones config fields.
constexpr const char *k_zone_config_zones = "zones";
constexpr const char *k_zone_config_name = "name";
constexpr const char *k_zone_config_id = "id";
constexpr const char *k_zone_config_points = "points";
constexpr const char *k_zone_config_role = "role";
constexpr const char *k_zone_config_event = "event";

}; // namespace ALBCommon

#endif // _ALB_COMMON_H