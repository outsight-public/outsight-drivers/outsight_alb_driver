#ifndef _ALB_HELPER_H
#define _ALB_HELPER_H

// ROS headers
#include "geometry_msgs/PoseStamped.h"
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/PointCloud2.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"

// Json parser
#include <nlohmann/json.hpp>

// Osef headers
#include "tlvCommon.h"

// Local headers
#include "outsight_alb_driver/AugmentedCloud.h"
#include "outsight_alb_driver/ObjectData.h"
#include "outsight_alb_driver/Zones.h"

/// Namespace to define helper functions.
namespace Helper
{
/// \brief Define the point fields for the ALB PointCloud2 message.
void define_point_fields(sensor_msgs::PointCloud2 &pointCloud);

/// \brief Define the PointCloud2 point data from raw points coming from the ALB.
void define_points_data(sensor_msgs::PointCloud2 &pointCloud, const uint32_t layers, const uint32_t points,
			float *pointData, bool use_colwise_order);

/// \brief Define a ROS Pose message from ALB position and rotation.
void define_pose(geometry_msgs::Pose &pose, const std::array<float, 3> &position, const std::array<float, 9> &rotation);

/// \brief Initialize a ROS Pose message.
void init_pose(geometry_msgs::Pose &pose);

/// \brief Parse the zone data in the raw string and set them in the Zones msg
void parse_zone_data(const std::string &raw_zones, outsight_alb_driver::Zones &zones_msg);

/// \brief Get a ROS pose from the TLV data.
/// \return
/// - True if Pose is defined.
/// - False on error.
bool get_pose_from_tlv(const Tlv::tlv_s *frame, geometry_msgs::PoseStamped &pose);

/// \brief Get a ROS egomotion pose from the TLV data.
/// \return
/// - True if Pose is defined.
/// - False on error.
bool get_egomotion_from_tlv(const Tlv::tlv_s *frame, geometry_msgs::PoseStamped &pose);

/// \brief Compute the odometry between the last two poses.
void computeOdometry(nav_msgs::Odometry &odom, const geometry_msgs::Pose &current_pose,
		     const geometry_msgs::Pose &last_pose, const geometry_msgs::Pose &first_pose, float dt);

/// \brief Get the string class of the tracked object from the ALB.
std::string get_object_class(uint32_t classId);

/// \brief Define the bounding box size for the tracked object.
void define_box_size(outsight_alb_driver::ObjectData &tracked, const Tlv::tlv_s *bBoxSizes, size_t objectIndex);

/// \brief Define the bounding box pose for the tracked object.
void define_box_pose(outsight_alb_driver::ObjectData &tracked, const Tlv::tlv_s *bBoxPoses, size_t objectIndex);

/// \brief Define the tracked object speed.
void define_object_speed(outsight_alb_driver::ObjectData &tracked, const Tlv::tlv_s *objectSpeeds, size_t objectIndex);

/// \brief Define the AugmentedCloud message.
void define_augmented_cloud(outsight_alb_driver::AugmentedCloud &message, const Tlv::tlv_s *frame);
}; // namespace Helper

#endif //_ALB_HELPER_H