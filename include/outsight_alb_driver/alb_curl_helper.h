#ifndef _ALB_CURL_HELPER_H
#define _ALB_CURL_HELPER_H

// curl headers
#include <curl/curl.h>

// Standard headers
#include <string>

// Local headers
#include "outsight_alb_driver/AlbConfig.h"
#include "outsight_alb_driver/AlbFile.h"

/// Class to handle the curl ALB requests.
class AlbCurlHelper {
    public:
	/// Processing services of the ALB.
	enum processing_service_e {
		Restart = 0,
		Stop,
		Status,
	};

	/// Processing configuration services.
	enum processing_config_e { Get = 0, Put };

	/// Storage services.
	enum storage_service_e {
		Download = 0,
		Upload,
		List,
	};

    public:
	AlbCurlHelper(const std::string &ip_address);
	~AlbCurlHelper(void);

	/// \brief Get the error message.
	const std::string getErrorMessage(void) const;

	/// \brief Execute the processing command.
	/// \return
	/// - True on success.
	/// - False on error.
	bool executeProcessing(const processing_service_e service);

	/// \brief Check if processing is running.
	/// \param running Output value, true if processing is running.
	/// \return
	/// - True on success.
	/// - False on error.
	bool isProcessingRunning(bool &running);

	/// \brief Execute the processing command for the configuration.
	/// \return
	/// - True on success.
	/// - False on error.
	bool executeProcessingConfig(const processing_config_e &service,
				     outsight_alb_driver::AlbConfig::Request &request);

	/// \brief Execute the processing command for the zones.
	/// \return
	/// - True on success.
	/// - False on error.
	bool executeProcessingZones(std::string &zones);

	/// \brief Execute the storage command.
	/// \return
	/// - True on success.
	/// - False on error.
	bool executeStorage(const storage_service_e service, outsight_alb_driver::AlbFile::Request &request);

    private:
	/// \brief Initialize the curl command.
	/// \return
	/// - True if curl has been initialized.
	/// - False on error.
	bool initCurl(void);

	/// \brief Define base curl command for the ALB.
	void defineBaseCommand(const std::string &alb_command);

	/// \brief Define the processing configuration command.
	/// \return
	/// - True on success.
	/// - False on error.
	bool defineProcessingConfig(const processing_config_e &service,
				    outsight_alb_driver::AlbConfig::Request &request);

	/// \brief Define the storage command.
	/// \return
	/// - True on success.
	/// - False on error.
	bool defineStorageCommand(const storage_service_e service, outsight_alb_driver::AlbFile::Request &request);

	/// \brief Execute the curl command.
	/// \return
	/// - True on success.
	/// - False on error.
	bool executeCurlCommand(const std::string &command);

	/// \brief Export the data to the output filepath.
	/// \return
	/// - True on success.
	/// - False otherwise.
	bool exportData(const std::string &data, const std::string &output_path,
			const std::string &print_text = std::string("Print data:"));

	/// \brief Read data from the input filepath.
	const std::string readData(const std::string &filepath);

	/// \brief Define the command to list storage files.
	bool defineStorageListCommand(const outsight_alb_driver::AlbFile::Request &request);

    protected:
	CURL *curl;
	struct curl_slist *chunk;
	long server_response;

	std::string curl_address;
	std::string callback_data;
	std::string error_message;

	FILE *file;
};

#endif // _ALB_CURL_HELPER_H