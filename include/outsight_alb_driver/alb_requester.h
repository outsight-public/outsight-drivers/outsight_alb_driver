#ifndef _ALB_REQUESTER_H
#define _ALB_REQUESTER_H

// ROS headers
#include "ros/ros.h"
#include "std_srvs/Trigger.h"

// Local headers
#include "outsight_alb_driver/AlbConfig.h"
#include "outsight_alb_driver/AlbFile.h"

/// Class to send requests to the ALB.
class AlbRequester {
    public:
	AlbRequester(ros::NodeHandle &node);

	/// \brief Initialize the ALB requester.
	/// \return
	/// - True if initialization is successful.
	/// - False on error.
	bool init(void);

    private:
	/// \brief Define the ALB services.
	void defineServices(ros::NodeHandle &node);

	/// \brief Service callback to start the processing.
	bool restartProcessingCallback(std_srvs::Trigger::Request &request, std_srvs::Trigger::Response &response);

	/// \brief Service callback to kill the processing.
	bool stopProcessingCallback(std_srvs::Trigger::Request &request, std_srvs::Trigger::Response &response);

	/// \brief Service callback to get the ALB configuration.
	bool getConfigCallback(outsight_alb_driver::AlbConfig::Request &request,
			       outsight_alb_driver::AlbConfig::Response &response);

	/// \brief Service callback to put the ALB configuration.
	bool putConfigCallback(outsight_alb_driver::AlbConfig::Request &request,
			       outsight_alb_driver::AlbConfig::Response &response);

	/// \brief Service callback to download a file from the ALB.
	bool downloadFileCallback(outsight_alb_driver::AlbFile::Request &request,
				  outsight_alb_driver::AlbFile::Response &response);

	/// \brief Service callback to upload a file to the ALB.
	bool uploadFileCallback(outsight_alb_driver::AlbFile::Request &request,
				outsight_alb_driver::AlbFile::Response &response);

	/// \brief Service callback to list files in the ALB.
	bool listFilesCallback(outsight_alb_driver::AlbFile::Request &request,
			       outsight_alb_driver::AlbFile::Response &response);

    private:
	std::string ip_address;

	ros::ServiceServer processing_service_restart;
	ros::ServiceServer processing_service_stop;
	ros::ServiceServer processing_service_get_config;
	ros::ServiceServer processing_service_put_config;

	ros::ServiceServer storage_service_download;
	ros::ServiceServer storage_service_upload;
	ros::ServiceServer storage_service_list;
};

#endif // _ALB_REQUESTER_H