#ifndef _ALB_READER_H
#define _ALB_READER_H

// Standard headers
#include <memory>

// Osef headers
#include "tcpStream.h"
#include "tlvParser.h"

// Local headers
#include "alb_publisher.h"

/// Class to read data from the ALB live stream.
class AlbReader {
    public:
	AlbReader(ros::NodeHandle &nodeHandle);

	/// \brief Initialize the parser.
	bool init();

	/// \brief Parse data from the input stream (ROS callback).
	/// \return
	/// - 0 on success.
	/// - -1 on error.
	int parse();

    private:
	/// \brief Check if the TLV is parsable.
	/// \return
	/// - True if parsing is possible.
	/// - False if not.
	bool isTlvParsable(const Tlv::tlv_s *frame);

	/// \brief Check if input arguments are valid.
	/// \return
	/// - True if arguments are valid.
	/// - False in case of error.
	bool areInputArgumentsValid();

	/// \brief Parse the ROS input arguments.
	void parseInputArguments();

	/// \brief Connect the parser to a live stream.
	/// \return
	/// - Error code.
	int connectToLiveStream();

    private:
	TcpStreamReader tcp_reader;
	std::string ip_v4;
	int port;

	AlbPublisher alb_publisher;
	std::unique_ptr<uint8_t[]> buffer;
	bool alb_initialized;
};

#endif // _ALB_READER_H