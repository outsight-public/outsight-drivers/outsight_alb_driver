# outsight_alb_driver

ROS1 package for Outsight Augmented Lidar Box (ALB).

**Author**: [Outsight](https://www.outsight.ai/)

## Installation
### Prerequisite

* ROS ([Install ROS](https://www.ros.org/install/))
* Functionnal ALB
* Install apt dependencies of the driver
  ```sh
  apt-get update
  # ROS Noetic release
  xargs apt-get install -y <apt-dependencies_noetic.txt
  # ROS Melodic release
  xargs apt-get install -y <apt-dependencies_melodic.txt
  ```

### Building from sources
1. Clone the repository
2. Build using the ROS catkin build system

```sh
cd catkin_ws/src
git clone https://gitlab.com/outsight-public/outsight-drivers/outsight_alb_driver.git
cd ../
catkin_make
source devel/setup.bash
```

### Configuration

* Configure the ALB according to your application [Documentation](https://docs.outsight.ai/).
* `ip_address` and `ip_port` have to be defined in the launch file (See `launch/outsight_alb.launch` for example).
* To be able to publish messages to the corresponding topics and frames, set up the `config/alb_tcp.yaml` file.

  To manage the frames, several frame names are defined in the configuration file:
    
    * `fixed_frame_id` defined with default value at `alb_fixed_frame` represents the first frame when the processing starts. It is the fixed reference frame.
    * `sensor_frame_id` represents the sensor frame (linked with sensor), with default value at `alb_sensor_frame`.
    * `base_frame_id` represents the robot base frame (this frame is optional, see the provided transformation section for more information)
    * `map_frame_id` is the fixed frame of the world and allows to link the odometry origin to the map (this frame is also optional).

## Usage

Run the two nodes defined, `alb_data` and `alb_services`.

```sh
roslaunch outsight_alb_driver outsight_alb.launch
```

To manage multiple configurations, the config file (placed in the dedicated /config folder) can be precised when launched.
```sh
roslaunch outsight_alb_driver outsight_alb.launch config_file:=custom_config.yaml
```

Recommendation: if ALB processing is manually resumed, restart ROS processing as well.

Why? It is preferable to reset the ROS node in order to account for any changes, such as if the processed record has been modified. Otherwise, you risk getting incorrect poses and odometry.

## Nodes
### alb_data

This node handles the received data from the ALB and publishes it to ROS.

#### Published topics
* `/alb/point_cloud` -> [sensor_msgs/PointCloud2](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/PointCloud2.html)
* `/alb/pose` -> [geometry_msgs/PoseStamped](http://docs.ros.org/en/noetic/api/geometry_msgs/html/msg/PoseStamped.html)
* `/alb/egomotion` -> [geometry_msgs/PoseStamped](http://docs.ros.org/en/noetic/api/geometry_msgs/html/msg/PoseStamped.html)
* `/alb/odometry` -> [nav_msgs/Odometry](http://docs.ros.org/en/noetic/api/nav_msgs/html/msg/Odometry.html)
* `/alb/time_reference` -> [sensor_msgs/TimeReference](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/TimeReference.html)
* `/alb/tracked_objects` -> [outsight_alb_driver/TrackedObjects](https://docs.ros.org/en/melodic/api/outsight_alb_driver/html/msg/TrackedObjects.html)
* `/alb/augmented_cloud` -> [outsight_alb_driver/AugmentedCloud](https://docs.ros.org/en/melodic/api/outsight_alb_driver/html/msg/AugmentedCloud.html)
* `/alb/zones` -> [outsight_alb_driver/Zones](To be added...)

#### Timestamps

Both ALB and ROS timestamps can be used to stamp the previous topic messages. The topic `/alb/time_reference` links the ALB timestamp and the ROS timestamp.
To use the ALB timestamp, set the `/use_alb_time` parameter to 'true' in the config file.

#### Profided transformation

If the transformation between the robot base frame and the sensor frame is provided (for instance within a URDF file), the package will broadcast the transform between the fixed frame (defined as the initial pose of the robot) and the robot base frame.
Otherwise, the transformation broadcasted is simply the transformation between the fixed frame and the sensor frame.
The tf broadcaster can be disabled by setting the `broadcast_tf` parameter to 'false' in the config file. 

The static transform between the map frame and fixed frame is always published. This transform is particularly useful in the case of relocalization in a reference map. This transform gives the link between the origin of the map and the first position where the processing starts. This parameter also sets the frame used for the Zones message.
When relocalization is disabled, `map_frame` and `fixed_frame` are the same.

With the default settings, the transform tree is structured like this:
            `map_frame -> fixed_frame -> base_frame -> sensor_frame`

#### Frames used for pose and odometry

For Pose, the coordinate frame used is:
- map frame, in case of Relocalization, or
- fixed frame, i.e. the initial Pose of the LiDAR, in case of Ego-Motion

In contrast, the odometry message, corresponding to the displacement between the first frame and the current frame, is always given in the fixed frame.  

### alb_services

This node defines services to interact with the ALB.

#### Custom services files

* [`outsight_alb_driver/AlbConfig`](https://docs.ros.org/en/melodic/api/outsight_alb_driver/html/srv/AlbConfig.html)
  
  Service to manage the processing configuration of the ALB. Used for getting and putting the configuration from/to the ALB.

* [`outsight_alb_driver/AlbFile`](https://docs.ros.org/en/melodic/api/outsight_alb_driver/html/srv/AlbFile.html)

  Service to use with the storage of the ALB, to download or upload files.

#### Services

For more information about the ALB API, please refer to the [API documentation](https://app.swaggerhub.com/apis-docs/Outsight/alb_api/4.0.0#/)

* `/alb/processing/restart` -> [std_srvs/Trigger](http://docs.ros.org/en/melodic/api/std_srvs/html/srv/Trigger.html)

  Service to restart the processing on the ALB. Trigger service does not require any arguments, you can just call it with the following command:
  ```sh
  rosservice call /alb/processing/restart
  ```

* `/alb/processing/stop` -> [std_srvs/Trigger](http://docs.ros.org/en/melodic/api/std_srvs/html/srv/Trigger.html)

  Service to stop the processing on the ALB.
  ```sh
  rosservice call /alb/processing/stop
  ```

* `/alb/processing/get_config` -> [outsight_alb_driver/AlbConfig](https://docs.ros.org/en/melodic/api/outsight_alb_driver/html/srv/AlbConfig.html)

  Service to get the processing configuration of the ALB. If argument is an empty string, configuration will be output in the roslaunch console. If argument is a full filepath, the configuration will be exported to this file.

  WARNING: Filepath has to be absolute.

  ```sh
  rosservice call /alb/processing/get_config ""
  ```
  ```sh
  rosservice call /alb/processing/get_config "/home/username/Documents/out_config.json"
  ```

* `/alb/processing/put_config` -> [outsight_alb_driver/AlbConfig](https://docs.ros.org/en/melodic/api/outsight_alb_driver/html/srv/AlbConfig.html)

  Service to put the processing configuration to the ALB. It has to be formatted as JSON format.

  ```sh
  rosservice call /alb/processing/put_config "/home/username/Documents/config_to_put.json"
  ```

* `alb/storage/download` -> [outsight_alb_driver/AlbFile](https://docs.ros.org/en/melodic/api/outsight_alb_driver/html/srv/AlbFile.html)

  Service to download a file from the ALB.
  ```sh
  # Call to download a map named map_name.ply on the ALB to the output file.
  rosservice call /alb/storage/download maps map_name.ply "/home/username/Documents/output_map.ply"

  # Same command with full argument description.
  rosservice call /alb/storage/download "category: 'maps' alb_filename: 'map_name.ply' filepath: '/home/username/Documents/output_map.ply'"
  ```

* `alb/storage/upload` -> [outsight_alb_driver/AlbFile](https://docs.ros.org/en/melodic/api/outsight_alb_driver/html/srv/AlbFile.html)

  Service to upload a file to the ALB.
  ```sh
  # Call to upload a map named map_name.ply to the ALB.
  rosservice call /alb/storage/upload maps map_name.ply "/home/username/Documents/input_map.ply"
  ```

* `alb/storage/list` -> [outsight_alb_driver/AlbFile](https://docs.ros.org/en/melodic/api/outsight_alb_driver/html/srv/AlbFile.html)

  Service to list the files on the ALB. 
  
  By default, the response is printed into the rosconsole. Set a `filepath` to export the response to a file. Note that the parameter `alb_filename` isn't used here.
  ```sh
  # List the map files on the ALB.
  rosservice call /alb/storage/list maps "" ""

  # List the map files on the ALB and export it.
  rosservice call /alb/storage/list maps "" "/home/username/maps_storage.json"
  ```

## Usage with other ROS messages

### LaserScan

For some applications, instead of the `PointCloud`, `LaserScan` can be useful. For this, we recommend using the ROS package [pointcloud_to_laserscan](http://wiki.ros.org/pointcloud_to_laserscan).

Then you can launch in another console the given launch file `outsight_to_laserscan.launch`, adjusting your own parameters. You can also integrate it directly in your own launch file for easier use.

```sh
roslaunch outsight_alb_driver outsight_to_laserscan.launch
```

## Unit testing

In this ROS driver, unit tests have been defined under the `test` directory. After any change, make sure the tests are still able to run.

```sh
catkin_make run_tests
catkin_test_results
```

This driver has been tested in CI under `melodic` and `noetic` releases.

## Contributing
### Bugs and issues
Please report bugs and/or issues using the [Issue Tracker](https://gitlab.com/outsight-public/outsight-drivers/outsight_alb_driver/-/issues)

### Feature requests and additions
In order to contribute to the code, please use Merge requests.

In case of Merge requests, the code will be integrated internally, in order to execute Continuous Integration and Continuous Deployment before being deployed publicly.